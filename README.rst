OpenLP Docker Runners
=====================

This repository holds the configuration to build the Docker runners for testing OpenLP.


.. note::

   The Dockerfiles contain version numbers to uniquely identify the release we're using. This helps us keep track of
   which version of software we're using, as well as allow for us to periodically update the software without resorting
   to tricks to work around our build process.
